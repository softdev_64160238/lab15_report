/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.charudet.reportproject;

import java.util.List;

/**
 *
 * @author User
 */
public class ArtistService {
    public List<ArtistReport> getArtistBYTotalPrice() {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistBYTotalPrice(10);
    }
    
    public List<ArtistReport> getArtistBYTotalPrice(String begin, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistBYTotalPrice(begin, end, 10);
    }
}
